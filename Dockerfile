# FROM php:7.4-fpm

# ARG user
# ARG uid

# WORKDIR /var/www/html
# COPY  travellist-laravel-demo .
# COPY config/000-default.conf /etc/apache2/sites-available/
# RUN apt-get update && apt-get upgrade -y
# RUN apt-get install -y unzip \
#     git \
#     curl \
#     libpng-dev \
#     libonig-dev \
#     libxml2-dev \
#     zip \
#     unzip

# # Clear cache
# RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# # Install PHP extensions
# RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# USER ${user}

FROM php:7.4.33-apache
WORKDIR /var/www/html
RUN apt update && \
    apt install -y zip unzip libonig-dev libpng-dev libxml2-dev git curl && \
    docker-php-ext-install -j$(nproc) mbstring pdo_mysql exif pcntl bcmath gd
COPY travellist-laravel-demo /var/www/html
COPY config/000-default.conf /etc/apache2/sites-available/
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN cd /var/www/html && \
    chown -R www-data:www-data /var/www/html/ && \
    COMPOSER_DISCARD_CHANGES=true composer update --no-interaction && \
    #chmod 666 /var/www/html/storage/logs/*.log && \
    php artisan key:generate && \
    php artisan config:cache



